Se realizaron dos casos y un excenario tal y como se me fue indicado, estos
casos fueron realizados en Katalon Studio y para replicarlos se debe realizar 
lo siguiente
              ***Caso #1***
Abrir el Navegador
Escribir la ruta  "https://www.wikipedia.org/"
Escribir la palabra "Prueba" En el buscador
Presionar el Boton Buscar
Verificar que en el Resultado se encuentre la palabra Prueba.

              ****Caso#2***
Abrir el Navegador
Escribir la ruta  "https://www.wikipedia.org/"
Hacer Click Sobre el link Commons 
Validar que el link te direcciona a una ventana con informacion acerca de los commons
Presionar el boton Upload
Validar que al presionar requiera que las personas se logeen
Presionar el link login 
Validar que el sistema accedio al loigin

Nota:
Para ver los codigos utilizados presionar Scripting
Para ver la parte teorica de los casos de pruebas presionar Test Case



