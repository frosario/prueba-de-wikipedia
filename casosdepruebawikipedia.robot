*** settings ***
Library   SeleniumLibrary

*** Variables***
${Browser}     Chrome
${Url}         https://www.wikipedia.org/


*** Test Cases ***
CA01 Validar funcionamiento de la barra de navegacion y el boton buscar.

   Open Browser    ${Url}   ${Browser}
   Input Text     xpath=//*[@id="searchInput"]    Prueba
   Click Button   xpath=//*[@id="search-form"]/fieldset/button
   Page Should Contain   Prueba



*** Test Cases ***
CA002 Validar funcionamientos de los links y botones
   Open Browser    ${Url}   ${Browser}
   Click link    xpath=//*[@id="www-wikipedia-org"]/div[10]/div[3]/div[1]/a
   Page Should Contain   Commons
   Click link   xpath=//a[@title="Special:UploadWizard"]
   Page Should Contain   Login required
   Close Browser

