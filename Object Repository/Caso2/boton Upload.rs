<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>boton Upload</name>
   <tag></tag>
   <elementGuidId>6e4ff3cd-fb94-4553-88dc-1168b544a1b9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#mw-content-text > div > div > table.mainpage-heading > tbody > tr > td.mainpage-actions > ul > li:nth-child(4) > a > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#mw-content-text > div > div > table.mainpage-heading > tbody > tr > td.mainpage-actions > ul > li:nth-child(4) > a > span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;mw-content-text&quot;]/div/div/table[1]/tbody/tr/td[2]/ul/li[4]/a/span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mw-ui-button mw-ui-progressive</value>
   </webElementProperties>
</WebElementEntity>
