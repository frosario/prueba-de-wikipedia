<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>link2</name>
   <tag></tag>
   <elementGuidId>a2d85320-533a-4225-be47-01e9fc56ac28</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#mw-content-text > p:nth-child(2) > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#mw-content-text > p:nth-child(2) > a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;mw-content-text&quot;]/p[2]/a</value>
   </webElementProperties>
</WebElementEntity>
