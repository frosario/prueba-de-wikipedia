<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Link</name>
   <tag></tag>
   <elementGuidId>144f0677-5da1-42ef-8059-c8267b126660</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#www-wikipedia-org > div.footer > div.other-projects > div:nth-child(1) > a > div.other-project-text > span.other-project-title.jsl10n</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#www-wikipedia-org > div.footer > div.other-projects > div:nth-child(1) > a > div.other-project-text > span.other-project-title.jsl10n</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;www-wikipedia-org&quot;]/div[10]/div[3]/div[1]/a/div[2]/span[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>other-project-title jsl10n</value>
   </webElementProperties>
</WebElementEntity>
