<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Boton Buscar</name>
   <tag></tag>
   <elementGuidId>9dc9caef-2fe3-487b-a915-e58141ea4389</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#search-form > fieldset > button > i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#search-form > fieldset > button > i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;search-form&quot;]/fieldset/button/i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sprite svg-search-icon</value>
   </webElementProperties>
</WebElementEntity>
